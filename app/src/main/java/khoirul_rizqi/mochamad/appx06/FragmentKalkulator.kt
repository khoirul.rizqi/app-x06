package khoirul_rizqi.mochamad.appx06

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_kali.*
import kotlinx.android.synthetic.main.frag_kali.view.*
import java.text.DecimalFormat

class FragmentKalkulator : Fragment() , View.OnClickListener{

    lateinit var thisParent: MainActivity
    lateinit var v : View
    var x : Double = 0.0
    var y : Double = 0.0
    var hasil : Double = 0.0

    override fun onClick(v: View?) {
        x = edX.text.toString().toDouble()
        y = edY.text.toString().toDouble()
        hasil = x*y
        txhasilkali.text = DecimalFormat("#.##").format(hasil)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v =  inflater.inflate(R.layout.frag_kali,container,false)
        v.kali.setOnClickListener(this)
        return v
    }
}